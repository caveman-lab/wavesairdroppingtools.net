﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WavesCS;

namespace WavesAirdroppingTools
{
    public partial class WavesAirdroper
    {
        public static void ValidateStatusForAirdrop(AssetDistributionStatusWithAirdropWeights status)
        {
            if (status.NumberOfValidHolders == null || status.NumberOfValidHolders <= 0)
            {
                throw new Exception("Status is invalid for airdrop: no valid holders.");
            }

            if (status.Holders.Any(l => l.Weight < 0))
            {
                throw new Exception("Status is invalid for airdrop: holder weight is invalid.");
            }

            if (Math.Round(status.Holders.Sum(l => l.Weight) * 1000000) != 1000000)
            {
                throw new Exception("Status is invalid for airdrop: holder weights do not sum up to 1.");
            }
        }

        public void ValidateBalanceForAirdrop(Dictionary<Asset, decimal> airdropAmounts, int numOfValidHolders)
        {
            if (airdropAmounts.Any(p => p.Value <= 0))
            {
                throw new Exception("Airdrop amount must be more than 0.");
            }

            var fee = GetEstimatedTransactionFee(numOfValidHolders, airdropAmounts.Count);

            lock (ApiLock)
            {
                var b = node.GetObject($"addresses/balance/details/{account.Address}").GetDecimal("available", Assets.WAVES);
                if (b < fee + (airdropAmounts.ContainsKey(Assets.WAVES) ? airdropAmounts[Assets.WAVES] : 0))
                {
                    throw new Exception("Waves amount is insufficient for airdrop.");
                }

                if (airdropAmounts.Any(p => p.Key != Assets.WAVES))
                {
                    var ab = node.GetAssetBalances(account.Address);
                    foreach (var p in airdropAmounts.Where(p => p.Key != Assets.WAVES))
                    {
                        var abp = ab.Keys.Where(a => a.Id.Equals(p.Key.Id)).FirstOrDefault();
                        if (abp != null && p.Value <= ab[abp]) continue;
                        else
                        {
                            throw new Exception($"{p.Key.Name} amount is insufficient for airdrop.");
                        }
                    }
                }
            }
        }

        public static decimal GetEstimatedTransactionFee(int numOfHolder, int numOfTokens = 1)
        {
            return (((numOfHolder / 100) + 1) * 0.001m + numOfHolder * 0.0005m) * numOfTokens;
        }

        private IEnumerable<WavesAirdropPayment> ProcessMassTransfer(List<MassTransferItem> transfers, Asset asset, int paidCount)
        {
            lock (ApiLock)
            {
                var tx = new MassTransferTransaction(Node.MainNetChainId, account.PublicKey, asset, transfers, $"{name} airdrop: {asset.Name} ({paidCount + 1} - {paidCount + transfers.Count})");
                tx.Sign(account);
                var id = node.Broadcast(tx).ParseJsonObject().GetString("id");

                return transfers.Select(t => new WavesAirdropPayment
                {
                    Recipient = t.Recipient,
                    TxId = id,
                    Asset = asset.Name,
                    Amount = t.Amount
                });
            }
        }

        private List<WavesAirdropPayment> ProcessPayment(AssetDistributionStatusWithAirdropWeights status, Asset asset, decimal amount)
        {
            List<WavesAirdropPayment> payments = new List<WavesAirdropPayment>();

            List<MassTransferItem> transfers = new List<MassTransferItem>();

            int paidCount = 0;

            foreach (var holder in status.Holders)
            {
                var amt = Math.Floor((holder.Weight * amount) * 100000000) / 100000000;
                if (amt > 0)
                {
                    transfers.Add(new MassTransferItem(holder.Address, amt));
                    if (transfers.Count == 100)
                    {
                        payments.AddRange(ProcessMassTransfer(transfers, asset, paidCount));

                        paidCount += 100;
                        transfers = new List<MassTransferItem>();

                        ApiWait();
                    }
                }
            }

            if (transfers.Count > 0)
            {
                payments.AddRange(ProcessMassTransfer(transfers, asset, paidCount));
            }

            return payments;
        }

        public void ValidatePayment(List<WavesAirdropPayment> payments)
        {
            lock (ApiLock)
            {
                Dictionary<string, MassTransferTransaction> txs = new Dictionary<string, MassTransferTransaction>();
                foreach (var payment in payments)
                {
                    if (string.IsNullOrWhiteSpace(payment.TxId))
                    {
                        throw new Exception($"Payment does not have transaction id.");
                    }

                    if (!txs.ContainsKey(payment.TxId))
                    {
                        txs[payment.TxId] = (MassTransferTransaction)node.GetTransactionById(payment.TxId);

                        ApiWait();
                    }

                    if (txs[payment.TxId].Transfers.Any(t => t.Recipient == payment.Recipient && t.Amount == payment.Amount))
                    {
                        // Looks good
                    }
                    else
                    {
                        throw new Exception($"Payment does not match. Address: {payment.Recipient}, Asset: {payment.Asset}, TxId: {payment.TxId}, Amount: {payment.Amount}");
                    }
                }
            }
        }

        private readonly object AirdropLock = new object();
        public WavesAirdropResult Airdrop(AssetDistributionStatusWithAirdropWeights status, Dictionary<Asset, decimal> airdropAmounts)
        {
            lock (AirdropLock)
            {
                var result = new WavesAirdropResult
                {
                    AssetDistributionStatus = status,
                    AirdropAmounts = airdropAmounts.Select(p => new KeyValuePair<string, decimal>(p.Key.Name, p.Value)).ToList(),
                    Payments = new List<WavesAirdropPayment>()
                };

                try
                {
                    ValidateStatusForAirdrop(status);

                    if (airdropAmounts.ContainsKey(Assets.WAVES)) airdropAmounts[Assets.WAVES] -= GetEstimatedTransactionFee(status.NumberOfValidHolders.Value, airdropAmounts.Count);
                    ValidateBalanceForAirdrop(airdropAmounts, status.NumberOfValidHolders.Value);

                    ApiWait();

                    // Payment
                    foreach (var amt in airdropAmounts)
                    {
                        result.Payments.AddRange(ProcessPayment(status, amt.Key, amt.Value));
                        ApiWait();
                    }

                    // Validate Result
                    System.Threading.Thread.Sleep(120000);  // Wait for 2 mins for the transactions to be confirmed
                    ValidatePayment(result.Payments);
                }
                catch (Exception ex)
                {
                    result.Error = ex;
                }

                return result;
            }
        }
    }
}