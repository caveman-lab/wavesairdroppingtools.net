﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WavesCS;

namespace WavesAirdroppingTools
{
    public class AssetDistributionStatus
    {
        public AssetDistributionStatus()
        {
            Holders = new List<AssetHolder>();
        }

        public AssetDistributionStatus(Asset asset, int height) : this()
        {
            this.Asset = asset;
            this.Height = height;
        }

        public Asset Asset { get; set; }
        public int Height { get; set; }
        public List<AssetHolder> Holders { get; set; }
    }

    public class AssetDistributionStatusWithAirdropWeights : AssetDistributionStatus
    {
        public AssetDistributionStatusWithAirdropWeights() : base() { }
        public AssetDistributionStatusWithAirdropWeights(Asset asset, int height) : base(asset, height) { }
        public AssetDistributionStatusWithAirdropWeights(AssetDistributionStatus status) : base(status.Asset, status.Height)
        {
            if (status is AssetDistributionStatusWithAirdropWeights) this.Holders = ((AssetDistributionStatusWithAirdropWeights)status).Holders;
            else this.Holders = status.Holders.Select(h => new AssetHolderWithAirdropWeight(h)).ToList();
        }

        public new List<AssetHolderWithAirdropWeight> Holders { get; set; }
        public int? NumberOfValidHolders { get => Holders?.Where(h => h.Weight > 0).Select(h => h.Address).Distinct().Count(); }
    }

    public class AssetHolder
    {
        public AssetHolder() { }

        public AssetHolder(string address, decimal amount) : this()
        {
            this.Address = address;
            this.Amount = amount;
        }

        public string Address { get; set; }
        public decimal Amount { get; set; }
    }

    public class AssetHolderWithAirdropWeight : AssetHolder
    {
        public AssetHolderWithAirdropWeight() : base() { }
        public AssetHolderWithAirdropWeight(string address, decimal amount) : base(address, amount) { }
        public AssetHolderWithAirdropWeight(AssetHolder holder) : base(holder.Address, holder.Amount)
        {
            if (holder is AssetHolderWithAirdropWeight) this.Weight = ((AssetHolderWithAirdropWeight)holder).Weight;
        }

        public decimal Weight { get; set; }
    }
}