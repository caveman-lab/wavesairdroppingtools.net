﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WavesCS;

namespace WavesAirdroppingTools
{
    public partial class WavesAirdroper
    {
        private readonly string name;
        private readonly Node node;
        private readonly PrivateKeyAccount account;
        private readonly bool isApiRateLimitExpected;

        public WavesAirdroper(string seed, string name = "Waves Airdroper", string apiHost = Node.MainNetHost, bool isApiRateLimitExpected = true)
        {
            this.account = PrivateKeyAccount.CreateFromSeed(seed, Node.MainNetChainId);
            this.name = name;
            this.node = new Node(apiHost, Node.MainNetChainId);
            this.isApiRateLimitExpected = isApiRateLimitExpected;
        }

        private readonly object ApiLock = new object();
        private void ApiWait()
        {
            // Avoid hitting API server rate limit on public nodes
            if (isApiRateLimitExpected)
            {
                System.Threading.Thread.Sleep(500);
            }
        }

        public AssetDistributionStatus GetAssetDistributionStatus(Asset asset)
        {
            lock (ApiLock)
            {
                var height = node.GetHeight() - 5;

                var path = $"assets/{asset.Id}/distribution/{height}/limit/10000";
                var after = string.Empty;

                AssetDistributionStatus status = new AssetDistributionStatus(asset, height);

                do
                {
                    ApiWait();
                    var response = node.GetObject(path + (string.IsNullOrEmpty(after) ? string.Empty : $"?after={after}"));
                    var items = response.GetObject("items");
                    foreach (var item in items)
                    {
                        status.Holders.Add(new AssetHolder(item.Key, items.GetDecimal(item.Key, asset)));
                    }
                    if (response.GetBool("hasNext")) after = response.GetString("lastItem");
                    else after = string.Empty;
                } while (!string.IsNullOrEmpty(after));

                return status;
            }
        }

        public AssetDistributionStatusWithAirdropWeights GetAssetDistributionForAirdrop(Asset asset, Dictionary<string, decimal> multipliers = null)
        {
            var status = GetAssetDistributionStatus(asset);

            List<AssetHolderWithAirdropWeight> holdersWithWeights = new List<AssetHolderWithAirdropWeight>();

            foreach (var holder in status.Holders)
            {
                holdersWithWeights.Add(new AssetHolderWithAirdropWeight(holder)
                {
                    Weight = holder.Amount * Math.Max(0, multipliers != null && multipliers.ContainsKey(holder.Address) ? multipliers[holder.Address] : 1)
                });
            }

            // Update weight to percentage
            var total = holdersWithWeights.Sum(l => l.Weight);
            holdersWithWeights.ForEach(h => { h.Weight = h.Weight / total; });

            return new AssetDistributionStatusWithAirdropWeights(status.Asset, status.Height)
            {
                Holders = holdersWithWeights
            };
        }
    }
}