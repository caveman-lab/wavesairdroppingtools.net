﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WavesAirdroppingTools
{
    public class WavesAirdropResult
    {
        public bool HasError { get => Error != null; }
        public Exception Error { get; set; }
        public List<KeyValuePair<string, decimal>> AirdropAmounts { get; set; }
        public List<WavesAirdropPayment> Payments { get; set; }
        public AssetDistributionStatusWithAirdropWeights AssetDistributionStatus { get; set; }
    }

    public class WavesAirdropPayment
    {
        public string Recipient { get; set; }
        public string TxId { get; set; }
        public string Asset { get; set; }
        public decimal Amount { get; set; }
    }
}