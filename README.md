## WavesAirdroppingTools.NET

Useful tools to help you airdrop tokens to your community

<br/>

### Getting Started

Clone the project and also the WavesCS project: 

https://github.com/wavesplatform/WavesCS

Build WavesCS and add both WavesCS.dll and Newtonsoft.Json.dll to the References of WavesAirdroppingTools project and build.

Add WavesAirdroppingTools.dll to your project's References and in your code as:

`using WavesAirdroppingTools;`

Create a WavesAirdroper object using your token sender's seed. Optionally you can use the API service of your own node.

**WAVES seed is sensitive data. Make sure you only use it in a safe working environment.**

`var airdroper = new WavesAirdroper(YOUR_SEED, OPTIONAL_NAME, OPTIONAL_HOST);`

Then you can use this object to check token distribution status or make airdrops easily.

<br/>

### Documentation

Get asset distribution status:

`var status = airdroper.GetAssetDistributionStatus(YOUR_ASSET); // Basic`

`var status = airdroper.GetAssetDistributionForAirdrop(YOUR_ASSET, OPTIONAL_MULTIPLIERS); // Use multipliers to modify holder weights, like skipping your own addresses`

Example of an airdrop:

```
var status = airdroper.GetAssetDistributionForAirdrop(Assets.CLMT);
var result = airdroper.Airdrop(status, new Dictionary<Asset, decimal> { { Assets.WAVES, 100 } });
```


<br/>

#### Prerequisites

Visual Studio 2017+


#### Language

C#


#### Author

Caveman


#### License

This project is licensed under the MIT License - see the LICENSE file for details
